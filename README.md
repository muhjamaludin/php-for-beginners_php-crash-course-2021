# Tutorial PHP for Beginners: PHP Crash Course 2021 #

### Prerequisite
- php 7.4.3

### How to run
- using built in web server `php -S localhost:<port>`
- open browser localhost:<port>/<file>

This tutorial inspired by udemy tutorial [PHP for Beginners: PHP Crash Course 2021](https://www.udemy.com/course/learn-php-for-beginners-php-crash-course-2021)

![PHP for Beginners: PHP Crash Course 2021](https://bitbucket.org/muhjamaludin/php-for-beginners_php-crash-course-2021/raw/51e602be441fdb04b5ff4f1c6e7916246e631bca/assets/images/php-beginners.png)
